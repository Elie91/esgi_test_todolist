<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $user = (new User())
            ->setName('Doe')
            ->setFirstname('Jhon')
            ->setEmail('jhondoe@gmail.com')
            ->setPassword('jhondoe26')
            ->setBirthday((new \DateTime())->sub(new \DateInterval('P20Y')));

        $user2 = (new User())
            ->setName('Bismuth')
            ->setFirstname('Elie')
            ->setEmail('bismuthelie@gmail.com')
            ->setPassword('jhondoe26')
            ->setBirthday((new \DateTime())->sub(new \DateInterval('P20Y')));

        $user3 = (new User())
            ->setName('Test')
            ->setFirstname('Duond')
            ->setEmail('testdupond@gmail.com')
            ->setPassword('jhondoe26')
            ->setBirthday((new \DateTime())->sub(new \DateInterval('P20Y')));

        $manager->persist($user);
        $manager->persist($user2);
        $manager->persist($user3);
        $manager->flush();
    }
}
