<?php

namespace App\DataFixtures;

use App\Entity\Todolist;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class TodolistFixtures extends Fixture implements DependentFixtureInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $users = $this->entityManager->getRepository(User::class)->findAll();
        $todolist = (new Todolist())
            ->setName('Test de todolist')
            ->setCreator($faker->randomElement($users));

        $todolist2 = (new Todolist())
            ->setName('Test de todolist 2')
            ->setCreator($faker->randomElement($users));

        $todolist3 = (new Todolist())
            ->setName('Test de todolist 3')
            ->setCreator($faker->randomElement($users));

        $manager->persist($todolist);
        $manager->persist($todolist2);
        $manager->persist($todolist3);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }

}
