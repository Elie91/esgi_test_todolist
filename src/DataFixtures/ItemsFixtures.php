<?php

namespace App\DataFixtures;

use App\Entity\Item;
use App\Entity\Todolist;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ItemsFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $todolists = $manager->getRepository(Todolist::class)->findAll();
        $items = [];
        for ($i = 0; $i < 15; $i++) {
            $items[] = (new Item())
                ->setName($faker->name)
                ->setContent($faker->sentence)
                ->setCreationDate($faker->dateTime)
                ->setTodolist($faker->randomElement($todolists));
        }
        foreach ($items as $item) {
            $manager->persist($item);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            TodolistFixtures::class
        ];
    }

}
