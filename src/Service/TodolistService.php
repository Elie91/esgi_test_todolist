<?php

namespace App\Service;


use App\Entity\Item;
use App\Entity\Todolist;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Exception;

class TodolistService
{
    private $entityManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->entityManager = $objectManager;
    }

    /**
     * @param User $user
     * @param Todolist $todolist
     * @return bool
     * @throws Exception
     */
    public function addTodolistToUser(User $user, Todolist $todolist)
    {
        if (!$user->canAddTodolist()) {
            throw new Exception("L'utilisateur possède déja une todolist");
        }
        $todolist->setCreator($user);
        $user->addTodolist($todolist);
        $this->entityManager->flush();
        return true;
    }

    /**
     * @param Item $newItem
     * @param Todolist $todolist
     * @return bool
     * @throws Exception
     */
    public function addItemToTodolist(Item $newItem, Todolist $todolist)
    {
        //La vérif des 10 items max (canAddItem()) est faite directement dans $todolist->addItem()

        //Vérifie que le dernier item ajouté na pas été ajouté il y'a moins de 30mn
        $item = $this->entityManager->getRepository(Item::class)->getLastItemCreated($todolist);
        if (!empty($item) && time() - $item->getCreationDate()->getTimestamp() < 30 * 60) {
            throw new Exception("Le dernier item à été crée il y'a moins de 30 minutes");
        }

        $todolist->addItem($newItem);
        $this->entityManager->flush();
        return true;
    }
}