<?php

namespace App\Service;

use App\Entity\User;
use DateTime;
use Exception;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class EmailService
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param string $from
     * @param User $user
     * @param string $subject
     * @param string $text
     * @return bool
     * @throws Exception
     */
    public function sendEmail(string $from, User $user, string $subject, string $text)
    {
        if(!$user->isValidBirthday18()) {
            return false;
        }

        $email = (new Email())
            ->from($from)
            ->to($user->getEmail())
            ->subject($subject)
            ->text($text);

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            throw new Exception($e);
        }

        return true;
    }
}
