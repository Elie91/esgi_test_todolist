<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemRepository")
 * @UniqueEntity("name")
 * @ApiResource(
 *     paginationItemsPerPage=5
 * )
 */
class Item
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;


    /**
     * @ORM\Column(type="string", length=1000)
     * @Assert\NotBlank
     */
    private $content;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank
     */
    private $creation_date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Todolist", inversedBy="items")
     */
    private $todolist;


    public function __construct()
    {
        $this->todolist = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        if($this->isContentValid($content)) {
            $this->content = $content;
            return $this;
        }
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creation_date;
    }

    public function setCreationDate(\DateTimeInterface $creation_date): self
    {
        $this->creation_date = $creation_date;

        return $this;
    }

    public function getTodolist(): ?Todolist
    {
        return $this->todolist;
    }

    public function setTodolist(?Todolist $todolist): self
    {
        $this->todolist = $todolist;

        return $this;
    }


    /**
     * @param string $content
     * @return bool
     */
    public function isContentValid(string $content)
    {
        if (strlen($content) > 1000) {
            return false;
        }
        return true;
    }

}
