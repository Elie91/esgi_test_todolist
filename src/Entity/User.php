<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="account")
 * @ApiResource()
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="date")
     */
    private $birthday;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Todolist", mappedBy="creator", orphanRemoval=true)
     */
    private $todolist;

    public function __construct()
    {
        $this->todolist = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return Collection|Todolist[]
     */
    public function getTodolist(): Collection
    {
        return $this->todolist;
    }

    public function addTodolist(Todolist $todolist): self
    {
        if ($this->canAddTodolist() && !$this->todolist->contains($todolist)) {
            $this->todolist[] = $todolist;
            $todolist->setCreator($this);
        }

        return $this;
    }

    public function removeTodolist(Todolist $todolist): self
    {
        if ($this->todolist->contains($todolist)) {
            $this->todolist->removeElement($todolist);
            // set the owning side to null (unless already changed)
            if ($todolist->getCreator() === $this) {
                $todolist->setCreator(null);
            }
        }

        return $this;
    }

    public function isValidName(): bool
    {
        if (empty($this->name) || empty($this->firstname)) {
            return false;
        }
        return true;
    }

    public function isValidEmail(): bool
    {
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }

    public function isValidPassword(): bool
    {
        if (strlen($this->password) < 8 || strlen($this->password) > 40) {
            return false;
        }
        return true;
    }

    public function isValidBirthday13(): bool
    {
        if ((new \DateTime())->diff($this->birthday, true)->y < 13)
            return false;

        return true;
    }

    public function isValidBirthday18(): bool
    {
        if ((new \DateTime())->diff($this->birthday, true)->y < 18)
            return false;

        return true;
    }

    public function isValid(): bool
    {
        $success = true;
        if (!$this->isValidName()) {
            $success = false;
        }
        if (!$this->isValidEmail()) {
            $success = false;
        }
        if (!$this->isValidPassword()) {
            $success = false;
        }
        if (!$this->isValidBirthday13()) {
            $success = false;
        }
        return $success;
    }

    public function canAddTodolist()
    {
        if (count($this->todolist) > 0) {
            return false;
        }
        return true;
    }
}
