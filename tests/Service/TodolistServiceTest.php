<?php

namespace App\Tests;

use App\Entity\Item;
use App\Entity\Todolist;
use App\Entity\User;
use App\Repository\ItemRepository;
use App\Repository\TodolistRepository;
use App\Repository\UserRepository;
use App\Service\TodolistService;
use PHPUnit\Framework\TestCase;

use Doctrine\Persistence\ObjectManager;


class TodolistServiceTest extends TestCase
{

    public function testAddTodolistToUserSuccess()
    {

        $objectManager = $this->createMock(ObjectManager::class);


        $user = (new User())
            ->setName('Jhon')
            ->setFirstname('Doe')
            ->setEmail('jhondoe@gmail.com')
            ->setPassword('jhondoedoe')
            ->setBirthday(new \DateTime());
        $userRepository = $this->createMock(UserRepository::class);
        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($user);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);


        $todolist = (new Todolist())
            ->setName('todolist');
        $todolistRepository = $this->createMock(TodolistRepository::class);
        $todolistRepository->expects($this->any())
            ->method('find')
            ->willReturn($todolist);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($todolistRepository);


        $todolistService = new TodolistService($objectManager);
        $this->assertEquals(true, $todolistService->addTodolistToUser($user, $todolist));

    }


    public function testAddTodolistToUserError()
    {

        $objectManager = $this->createMock(ObjectManager::class);


        $user = (new User())
            ->setName('Jhon')
            ->setFirstname('Doe')
            ->setEmail('jhondoe@gmail.com')
            ->setPassword('jhondoedoe')
            ->setBirthday(new \DateTime());


        $todolist = (new Todolist())
            ->setName('todolist')
            ->setCreator($user);
        $todolistRepository = $this->createMock(TodolistRepository::class);
        $todolistRepository->expects($this->any())
            ->method('find')
            ->willReturn($todolist);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($todolistRepository);


        //ajout de la todolist au user
        $user->addTodolist($todolist);

        $userRepository = $this->createMock(UserRepository::class);
        $userRepository->expects($this->any())
            ->method('find')
            ->willReturn($user);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($userRepository);


        $todolistService = new TodolistService($objectManager);
        $this->expectExceptionMessage("L'utilisateur possède déja une todolist");
        $todolistService->addTodolistToUser($user, $todolist);

    }


    public function testAddItemToTodolistError()
    {

        $objectManager = $this->createMock(ObjectManager::class);


        //L'item est crée au moment du test
        $item = (new Item())
            ->setName('item')
            ->setContent('lorem ipsum')
            ->setCreationDate(new \DateTime());
        $itemRepository = $this->createMock(ItemRepository::class);
        $itemRepository->expects($this->any())
            ->method('getLastItemCreated')
            ->willReturn($item);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($itemRepository);


        $todolist = (new Todolist())
            ->setName('todolist');
        $todolistRepository = $this->createMock(TodolistRepository::class);
        $todolistRepository->expects($this->any())
            ->method('find')
            ->willReturn($todolist);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($todolistRepository);


        $todolistService = new TodolistService($objectManager);

        $this->expectExceptionMessage("Le dernier item à été crée il y'a moins de 30 minutes");
        $todolistService->addItemToTodolist($item, $todolist);

    }


    public function testAddItemToTodolistSuccess()
    {

        $objectManager = $this->createMock(ObjectManager::class);

        //L'item est crée au moment du test avec -30 mn sur sa date de création
        $item = (new Item())
            ->setName('item')
            ->setContent('lorem ipsum')
            ->setCreationDate((new \DateTime())->sub(new \DateInterval('PT30M')));

        $itemRepository = $this->createMock(ItemRepository::class);
        $itemRepository->expects($this->any())
            ->method('getLastItemCreated')
            ->willReturn($item);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($itemRepository);


        $todolist = (new Todolist())
            ->setName('todolist');
        $todolistRepository = $this->createMock(TodolistRepository::class);
        $todolistRepository->expects($this->any())
            ->method('find')
            ->willReturn($todolist);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($todolistRepository);


        $todolistService = new TodolistService($objectManager);

        $this->assertEquals(true, $todolistService->addItemToTodolist($item, $todolist));

    }
}
