<?php

namespace App\Tests;

use App\Entity\User;
use App\Service\EmailService;
use Doctrine\Persistence\ObjectManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;


class EmailServiceTest extends TestCase
{

    private $user;

    public function __construct()
    {
        parent::__construct();
        $this->user = (new User())
            ->setName('bismuth')
            ->setFirstname('elie')
            ->setPassword('elieleie')
            ->setEmail('elieelie@gmail.com')
            ->setBirthday((new \DateTime())->sub(new \DateInterval('P20Y')));
    }

    public function testSendEmailSuccess()
    {
        $mailer = $this->createMock(MailerInterface::class);
        $emailService = new EmailService($mailer);
        $this->assertEquals(true, $emailService->sendEmail("jhondoe@gmail.com", $this->user, "test envoie email", "ceci est un test"));

    }

    public function testSendEmailFalse()
    {
        $this->user->setBirthday((new \DateTime())->sub(new \DateInterval('P17Y')));
        $mailer = $this->createMock(MailerInterface::class);
        $emailService = new EmailService($mailer);
        $this->assertEquals(false, $emailService->sendEmail("jhondoe@gmail.com", $this->user, "test envoie email", "ceci est un test"));

    }

}