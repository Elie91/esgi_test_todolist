<?php

namespace App\Tests\Entity;

use App\Entity\Item;
use PHPUnit\Framework\TestCase;


class ItemTest extends TestCase
{

    //L'item unique est vérifié par l'annotation @uniqueEntity dans la classe

    public function testIsContentValidSuccess()
    {
        $item = (new Item())
            ->setName('Faire à manger')
            ->setContent('Il est important de manger convenablement')
            ->setCreationDate(new \DateTime());
        $content = "ceci est un test d'ajout";
        $this->assertEquals(true, $item->isContentValid($content));

    }

    public function testIsContentValidError()
    {
        $item = (new Item())
            ->setName('Faire à manger')
            ->setContent('Il est important de manger convenablement')
            ->setCreationDate(new \DateTime());
        $content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ItLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It";
        $this->assertEquals(false, $item->isContentValid($content));
    }


}
