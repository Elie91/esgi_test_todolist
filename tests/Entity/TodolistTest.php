<?php

namespace App\Tests;

use App\Entity\Item;
use App\Entity\Todolist;
use App\Entity\User;
use PHPUnit\Framework\TestCase;


class TodolistTest extends TestCase
{
    private $todolist;
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->todolist = (new Todolist())
            ->setName('Ma todolist');

        for ($i = 0; $i < 10; $i++) {
            $item = (new Item())
                ->setName('lorem')
                ->setContent('loremipsum')
                ->setCreationDate(new \DateTime());
            $this->todolist->addItem($item);
        }
    }

    public function testAddItem()
    {
        $item = (new Item())
            ->setName('lorem')
            ->setContent('loremipsum')
            ->setCreationDate(new \DateTime());
        //on essaye d'ajouter un 11eme item a la liste
        $this->todolist->addItem($item);
        //le nombre d'items reste a 10
        $this->assertCount(10, $this->todolist->getItems());
    }

    public function testCanAddItem()
    {
        $item = (new Item())
            ->setName('lorem')
            ->setContent('loremipsum')
            ->setCreationDate(new \DateTime());
        $this->assertEquals(false, $this->todolist->canAddItem($item));
    }
}
