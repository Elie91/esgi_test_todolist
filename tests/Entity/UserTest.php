<?php

namespace App\Tests\Entity;
use App\Entity\Todolist;
use App\Entity\User;
use PHPUnit\Framework\TestCase;


class UserTest extends TestCase
{
    private $user;
    private $todolist;

    public function __construct()
    {
        parent::__construct();
        $this->todolist = (new Todolist())
            ->setName('Test todolist');

        $this->user = (new User())
            ->setEmail('email@email.com')
            ->setName('doe')
            ->setFirstname('jhon')
            ->setPassword('loremipsum')
            ->setBirthday((new \DateTime())->sub(new \DateInterval('P20Y')))
            ->addTodolist($this->todolist);
    }

    public function testIsValid()
    {
        $this->assertEquals(true, $this->user->isValid());
    }

    public function testIsValidName()
    {
        $this->user->setName('');
        $this->assertEquals(false, $this->user->isValidName());
    }

    public function testIsValidEmail()
    {
        $this->user->setEmail('loremipsum.com');
        $this->assertEquals(false, $this->user->isValidEmail());
    }

    public function testIsValidPassword()
    {
        $this->user->setPassword('lore');
        $this->assertEquals(false, $this->user->isValidPassword());
    }

    public function testIsValidBirthday13()
    {
        $this->user->setBirthday((new \DateTime())->sub(new \DateInterval('P10Y')));
        $this->assertEquals(false, $this->user->isValidBirthday13());
    }


    public function testCanAddTodolist()
    {
        $this->assertEquals(false, $this->user->canAddTodolist());
    }

}
