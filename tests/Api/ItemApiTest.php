<?php

namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Item;

class ItemApiTest extends ApiTestCase
{
    public function testGetItems()
    {
        $response = static::createClient()->request('GET', '/api/items');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertEquals(16, $response->toArray()['hydra:totalItems']);
        $this->assertJsonContains([
            '@context' => '/api/contexts/Item',
            '@id' => '/api/items',
            '@type' => 'hydra:Collection',
            'hydra:totalItems' => 15,
            'hydra:view' => [
                '@id' => '/api/items?page=1',
                '@type' => 'hydra:PartialCollectionView',
                'hydra:first' => '/api/items?page=1',
                'hydra:last' => '/api/items?page=3',
                'hydra:next' => '/api/items?page=2',
            ],
        ]);
        $this->assertMatchesResourceCollectionJsonSchema(Item::class);
    }

    public function testCreateItem(): void
    {
        $response = static::createClient()->request('POST', '/api/items', ['json' => [
            'name' => 'test api platform',
            'content' => "Brilliantly conceived and executed, this powerful evocation of twenty-first century America gives full rein to Margaret Atwood",
            'creationDate' => "2020-05-25T00:00:00+00:00",
            "todolist" => "api/todolists/1"
        ]]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            '@context' => '/api/contexts/Item',
            '@type' => 'Item',
            "name" => "test api platform",
            "content" => "Brilliantly conceived and executed, this powerful evocation of twenty-first century America gives full rein to Margaret Atwood",
            "creationDate" => "2020-05-25T00:00:00+00:00",
            "todolist" => "api/todolists/1"
        ]);
        $this->assertMatchesResourceItemJsonSchema(Item::class);
    }

    public function testCreateInvalidItem(): void
    {
        static::createClient()->request('POST', '/api/items', ['json' => [
            'name' => '',
        ]]);
        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            '@context' => '/api/contexts/ConstraintViolationList',
            '@type' => 'ConstraintViolationList',
            'hydra:title' => 'An error occurred',
            'hydra:description' => 'name: This value should not be blank.\ncontent: This value should not be blank.\ncreation_date: This value should not be blank.'
        ]);
    }
}